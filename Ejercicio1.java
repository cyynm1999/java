public class Ejercicio1 {
	public static void main(String args[])	{

		if (args.length!=3) {
			System.out.println("Dato incorrecto");
			System.exit(0);
		} else {
			System.out.println("Has insertado tres números");
		}
		int num1=Integer.parseInt(args[0]);
		int num2=Integer.parseInt(args[1]);
		int num3=Integer.parseInt(args[2]);

		if (num1>num2 && num1>num3 && num2<num3) {
			System.out.println("El número mayor es " +num1);
			System.out.println("El número menor es " +num2);
		} else if (num1>num2 && num1>num3 && num3<num2) {
			System.out.println("El número mayor es " +num1);
			System.out.println("El número menor es " +num3);
		} else if (num2>num1 && num2>num3 && num1<num3) {
			System.out.println("El número mayor es " +num2);
			System.out.println("El número menor es " +num1);
		} else if (num2>num1 && num2>num3 && num3<num1) {
			System.out.println("El número mayor es " +num2);
			System.out.println("El número menor es " +num3);
		} else if (num3>num1 && num3>num2 && num1<num2) {
			System.out.println("El número mayor es " +num3);
			System.out.println("El número menor es " +num1);
		} else if (num3>num1 && num3>num2 && num2<num1) {
			System.out.println("El número mayor es " +num3);
			System.out.println("El número menor es " +num2);
		} else {
			System.out.println("Escribe un número válido");
		}




	}
}
