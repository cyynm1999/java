public class Semana {
	public static void main(String args[]) {
		String diasemana=(args[0]);
		switch (diasemana) {
			case "uno": System.out.println("Lunes"); break;
			case "dos": System.out.println("Martes"); break;
			case "tres": System.out.println("Miércoles"); break;
			case "cuatro": System.out.println("Jueves"); break;
			case "cinco": System.out.println("Viernes"); break;
			case "seis": System.out.println("Sábado"); break;
			case "siete": System.out.println("Domingo"); break;
			default: System.out.println("No válido");
		}

	}
}