public class Ejercicio3 {

	public static void main(String args[]) {
	double a, b, c;

	a=Double.parseDouble(args[0]);
	b=Double.parseDouble(args[1]);
	c=Double.parseDouble(args[2]);

	if (args.length!=3) {
		System.out.println("Incorrecto");
		System.exit(0);
	} else {
		double x1 = (-b + Math.sqrt((b*b)-(4*a*c)))/(2*a);
		double x2 = (-b - Math.sqrt((b*b)-(4*a*c)))/(2*a);

		System.out.println("La solucion de x1: "+x1);
    	System.out.println("La solucion de x2: "+x2);
	}

}



}
