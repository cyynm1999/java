public class Mes {

public static void main(String args[]) {
		String mesesdelaño=(args[0]);
		switch(mesesdelaño) {
			case "uno": System.out.println("Enero"); break;
			case "dos": System.out.println("Febrero"); break;
			case "tres": System.out.println("Marzo"); break;
			case "cuatro": System.out.println("Abril"); break;
			case "cinco": System.out.println("Mayo"); break;
			case "seis": System.out.println("Junio"); break;
			case "siete": System.out.println("Julio"); break;
			case "ocho": System.out.println("Agosto"); break;
			case "nueve": System.out.println("Septiembre"); break;
			case "diez": System.out.println("Octubre"); break;
			case "once": System.out.println("Noviembre"); break;
			case "doce": System.out.println("Diciembre"); break;
			default: System.out.println("No es válido");
		}
	}	
}