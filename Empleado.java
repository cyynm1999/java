public class Empleado {
	String dni;
	String nombre;
	String apellidos;
	double sueldo;
	double irpf;
	public Empleado(String dni, String nombre, String apellidos, double sueldo, double irpf) {
		this.dni=dni;
		this.nombre=nombre;
		this.apellidos=apellidos;
		this.sueldo=sueldo;
		this.irpf=irpf;
	}
	public double calcularIrpf() {
		double irpf;
		if (sueldo>=10000) {
			irpf=25;
		} else if (sueldo>=9000 && sueldo<10000) {
			irpf=20;
		} else if (sueldo>=8000 && sueldo<9000) {
			irpf=15;
		} else if (sueldo>=7000 && sueldo<8000) {
			irpf=12;
		} else if (sueldo>=0    && sueldo<7000) {
			irpf=10;
		} else {
			irpf=-1;
		}
		return irpf;
	}
	public static void main(String args[]) {
		Empleado e1=new Empleado("12345678Z","María José","López López",12000,0);
		Empleado e2=new Empleado("23456789A","Antonio Luis","Fernández García",9500,0);
		Empleado e3=new Empleado("45678901B","Fernanda","García Machista",8500,0);
		Empleado e4=new Empleado("56789012C","Fernando","Hernández López",7500,0);
		Empleado e5=new Empleado("67890123D","Javier","Pérez Pérez",6500,0);
		Empleado e6=new Empleado("78901234E","Javiera","García Martínez",-6500,0);
		System.out.println("irpf e1: "+e1.calcularIrpf());
		System.out.println("irpf e2: "+e2.calcularIrpf());
		System.out.println("irpf e3: "+e3.calcularIrpf());
		System.out.println("irpf e4: "+e4.calcularIrpf());
		System.out.println("irpf e5: "+e5.calcularIrpf());
		System.out.println("irpf e6: "+e6.calcularIrpf());

	}
}