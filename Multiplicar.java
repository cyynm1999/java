public class Multiplicar {
	public static void main(String args[]) {

		int numero=Integer.parseInt(args[0]);
		int i=1;
		while(i<=10) { // i vale 1,
			System.out.println(numero + " * " + i + " = " + (numero*i));
			i++;
		}

	}
}